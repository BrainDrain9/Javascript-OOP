function Person(age,weight){
 this.age = age;
 this.weight = weight;
}

Person.prototype.getInfo = function(){
 return "I am " + this.age + " years old " +
    "and weighs " + this.weight +" kilo.";
};

function Student(age,weight,salary){
 this.age = age;
 this.weight = weight;
 this.salary = salary;
}
Student.prototype = new Person();

Student.prototype.getInfo = function(){
 return "I am " + this.age + " years old " +
    "and weighs " + this.weight +" kilo " +
    "and earns " + this.salary + " dollar.";  
};

var person = new Person(50,90);
var employee = new Student(43,80,50000);

console.log(person.getInfo());
console.log(employee.getInfo());

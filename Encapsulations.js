function MyClass() {
  var hidden = 0;
  this.myMethod = function() {
    hidden++;
    return hidden;
  }
}

MyClass.prototype.anotherMethod = function() {
    this.hidden++;
    return this.hidden;
}

var myInstance = new MyClass();
myInstance.myMethod(); // 1
myInstance.anotherMethod(); // Type error: "hidden is undefined"
myInstance.hidden; // "undefined"
myInstance.hidden = 0; // 0
myInstance.myMethod(); // 2
myInstance.hidden; // 0
myInstance.anotherMethod(); // 1

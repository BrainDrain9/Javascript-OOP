
function Animal(name) {
  this.name = name;
  this.speed = 0;
}


Animal.prototype.run = function() {
  alert(this.name + " run!")
}


function Rabbit(name) {
  Animal.apply(this, arguments);
}


Rabbit.prototype = Object.create(Animal.prototype);

Rabbit.prototype.constructor = Rabbit;

Rabbit.prototype.run = function() {
  
  Animal.prototype.run.apply(this);
  alert( this.name + " jump!" );
};


var rabbit = new Rabbit('Rabbit');
rabbit.run();
